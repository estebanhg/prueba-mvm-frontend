
import Navbar from './components/Navbar'
import {BrowserRouter as Router, Route} from 'react-router-dom'
import CrearUsuarios from './components/CrearUsuarios'
import Usuarios from './components/Usuarios';


function App() {
  return (
    <Router>
     <Navbar/>
    <Route path='/' exact component ={Usuarios}/>
    <Route path='/crear' component ={CrearUsuarios}/>
    <Route path='/actualizar/:id' component ={CrearUsuarios}/>
    </Router>
  );
}

export default App;
