import React, { useState, useEffect } from 'react'
import {Link} from 'react-router-dom'
import Axios from 'axios'

export default function Usuarios() {


    const [datos,setDatos]=useState([])


    useEffect(()=>{

            obtenerUsuarios();
    },[])


    const obtenerUsuarios = async() =>{

        const respuesta = await Axios.get('http://localhost:4000/usuarios/listar')
        setDatos(respuesta.data)
        //console.log(respuesta)
    }

    const eliminarUsuarios = async(id)=>{

        const respuesta = await Axios.delete('http://localhost:4000/usuarios/eliminar/'+id)
        window.location.href='/'
        //console.log(respuesta)
    }

    return (
        <div className="container mt-4">
            
            <div className="card-header text-center">
                <h4>Usuarios Registrados</h4>
            </div>
            <table className="table">
                <thead className="thead-dark">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Apellido</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Telefono</th>
                    <th scope="col">Acciones</th>
                    </tr>
                </thead>

                {
                    datos.map((usuarios,i)=>(
                        <tbody key={usuarios.id}>
                            <tr>
                                <th>{i+1}</th>
                                <th>{usuarios.nombre}</th>
                                <td>{usuarios.apellido}</td>
                                <td>{usuarios.correo}</td>
                                <td>{usuarios.telefono}</td>
                                <td>
                                    <Link type="button" className="btn btn-info mr-2" to={'actualizar/'+usuarios.id}>
                                        Actualizar</Link>
                                    <button type="button" className="btn btn-danger"
                                    onClick={()=>(eliminarUsuarios(usuarios.id))}>Eliminar</button>
                                </td>
                            </tr>
                        
                        </tbody>
                    ))
                }
                
            </table>
              
        </div>
    )
}
