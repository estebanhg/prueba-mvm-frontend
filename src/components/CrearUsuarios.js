import React, { useState, useEffect } from 'react'
import Axios from 'axios'

export default function CrearUsuarios(props) {

    const [nombre,setNombre] = useState('')
    const [apellido,setApellido] = useState('')
    const [correo,setCorreo] = useState('')
    const [telefono,setTelefono] = useState('')
    const [modificar,setModificar] = useState(false)


    useEffect(()=>{

        if(props.match.params.id){
            setModificar(true)
            const id=props.match.params.id

            consultarUsuario(id)
        }

    },[props.match.params.id])

    const consultarUsuario = async(id)=>{
        const respuesta= await Axios.get('http://localhost:4000/usuarios/listarunico/'+id)
        //console.log(respuesta)

        setNombre(respuesta.data.nombre)
        setApellido(respuesta.data.apellido)
        setCorreo(respuesta.data.correo)
        setTelefono(respuesta.data.telefono)

    }


    const guardar = async()=> {

        
        const usuarioNuevo = {
            nombre:nombre, 
            apellido:apellido, 
            correo:correo,
            telefono: telefono
        }

        
        const respuesta = await Axios.post('http://localhost:4000/usuarios/crear',usuarioNuevo)
        //console.log(respuesta)
        window.location.href='/'
    }


    const actualizar = async()=>{

        const id=props.match.params.id 

        const usuario ={
            nombre:nombre,
            apellido:apellido,
            correo:correo,
            telefono:telefono
        }

        const respuesta = await Axios.put('http://localhost:4000/usuarios/actualizar/'+id,usuario)

        //console.log(respuesta)
        window.location.href='/'
    }

    const accion=async(e)=>{
        e.preventDefault()

        if(modificar){
            actualizar()
        }else {
            guardar()
        }
    }
   
    return (
        <div className="container text-center">
            <div className="col-md-8 pt-8 mx-auto">
                <div className="card card-body">
                    <h4>Registrar nuevo usuario</h4>

                    <form onSubmit={accion}>
                        <div className="form-group">
                            <input type="text" className="form-control" value={nombre} 
                            onChange={e =>setNombre(e.target.value)} placeholder="Nombre: " required/>
                        </div>
                       
                        <div className="form-group">
                            <input type="text" className="form-control" value={apellido} 
                            onChange={e =>setApellido(e.target.value)}placeholder="Apellido: " required/>
                        </div>
                        <div className="form-group">
                            <input type="email" className="form-control" value={correo} 
                            onChange={e =>setCorreo(e.target.value)}placeholder="Correo: " required/>
                        </div>
                        <div className="form-group">
                            
                            <input type="text" className="form-control" value={telefono} 
                            onChange={e =>setTelefono(e.target.value)}placeholder="Telefono: " required/>
                        </div>

                        {
                            modificar ? <button type="submit" className="btn btn-warning">
                                Actualizar</button>:
                                <button type="submit" className="btn btn-success">Guardar</button>
                        }

                        
                    </form>
                </div>

            </div>
        </div>
    )
}
